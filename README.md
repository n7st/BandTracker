# BandTracker

BandTracker is a command line application that helps you track which bands
you've seen. It requires an InfluxDB database to write data to, which can then
be read using analytics programs of your choice.

## Features

* Store venue names and locations. Venue names must be unique (or they'll be
  overwritten).
* Add gigs against venues and festivals.

## Installation

### From source

Requires a properly configured Go development environment.

```
% go install gitlab.com/n7st/bandtracker
% bandtracker
```

## Analytics

I recommend using [Grafana](https://grafana.com/) to analyse data stored by
BandTracker.