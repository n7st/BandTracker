package main

import "gitlab.com/n7st/bandtracker/internal/app/bandtracker"

func main() {
	bt := bandtracker.NewBandTracker()

	defer func() {
		if err := bt.DB.Local.Close(); err != nil {
			panic(err)
		}
	}()

	defer func() {
		if err := bt.DB.InfluxDB.Close(); err != nil {
			panic(err)
		}
	}()

	bt.Run()
}
