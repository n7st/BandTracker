module gitlab.com/n7st/bandtracker

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/influxdata/influxdb v1.7.9
	github.com/shibukawa/configdir v0.0.0-20170330084843-e180dbdc8da0
	go.etcd.io/bbolt v1.3.3
	golang.org/x/sys v0.0.0-20191029155521-f43be2a4598c // indirect
	gopkg.in/yaml.v2 v2.2.4
)
