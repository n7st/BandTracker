package command

import (
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/n7st/bandtracker/internal/pkg/store"
)

// Festival describes a music festival. It can be created from command line
// arguments or loaded from the Bolt database.
type Festival struct {
	*Command

	Festival *store.Festival
}

// Add creates a new festival in the local Bolt database
func (f *Festival) Add() {
	if f.Festival.Name == "" {
		fmt.Println("name is required")
		os.Exit(1)
	}

	festival, err := f.DB.GetFestival(f.Festival.Name)

	if err != nil {
		panic(err) // TODO
	}

	if festival == nil {
		err = f.DB.SetFestival(f.Festival)

		if err != nil {
			panic(err) // TODO
		}

		festival = f.Festival
	}

	start, end := f.Festival.GetDates()

	fmt.Printf("[festival] name: %s, start: %s, end: %s\n",
		f.Festival.Name,
		start,
		end,
	)
}

func (f *Festival) BulkAdd() {
	validateBulkAdd(f.Command.BulkFileName)

	rows := getCSV(f.Command.BulkFileName)

	for i, line := range rows {
		if i == 0 {
			continue
		}

		var start, end *time.Time

		start = parseUserDate(line[1])
		end = parseUserDate(line[2])

		festival := &Festival{
			Command: f.Command,
			Festival: &store.Festival{
				Name:  line[0],
				Start: start,
				End:   end,
			},
		}

		festival.Add()
	}
}

func (f *Festival) Delete() {
}

func (f *Festival) Edit() {
}

func (f *Festival) Parse() {
	var rawStartDate, rawEndDate string

	cmd := flag.NewFlagSet("festival add", flag.ExitOnError)
	f.Festival = &store.Festival{}

	cmd.StringVar(&f.Festival.Name, "name", "", "the festival's name")
	cmd.StringVar(&rawStartDate, "start", "", fmt.Sprintf("the festival's start date (%s)", store.HintDateFormat))
	cmd.StringVar(&rawEndDate, "end", "", fmt.Sprintf("the festival's end date (%s)", store.HintDateFormat))
	cmd.StringVar(&f.Command.BulkFileName, "csv_filename", "", "bulk CSV data")

	err := cmd.Parse(os.Args[3:])

	if err != nil {
		panic(err)
	}

	f.Festival.Start = parseUserDate(rawStartDate)
	f.Festival.End = parseUserDate(rawEndDate)
}
