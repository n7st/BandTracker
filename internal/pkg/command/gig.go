package command

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/influxdata/influxdb/client/v2"

	"gitlab.com/n7st/bandtracker/internal/pkg/store"
)

// Gig describes a gig at a Venue or Festival. It can be created from command
// line arguments.
type Gig struct {
	*Command

	Festival *store.Festival
	Venue    *store.Venue

	Bands  []string
	Date   *time.Time
	Rating float64
}

// Add creates a gig in the remote InfluxDB database
func (g *Gig) Add() {
	// Ratings are between 0 and 5
	if g.Rating > 5 {
		g.Rating = 5
	} else if g.Rating < 0 {
		g.Rating = 0
	}

	venue, err := g.Command.DB.GetVenue(g.Venue.Name)

	if err != nil {
		panic(err)
	}

	if venue != nil {
		g.Venue = venue
	}

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Precision: "s",
		Database:  g.Command.InfluxDBName,
	})

	if err != nil {
		panic(err)
	}

	for _, b := range g.Bands {
		tags := make(map[string]string)
		fields := make(map[string]interface{})

		tags["band_name"] = b

		fields["band_name"] = b
		fields["rating"] = g.Rating

		if g.Venue != nil {
			tags["venue_name"] = g.Venue.Name
			tags["venue_latitude"] = fmt.Sprintf("%.2f", g.Venue.Latitude)
			tags["venue_longitude"] = fmt.Sprintf("%.2f", g.Venue.Longitude)

			fields["venue_name"] = g.Venue.Name
			fields["venue_latitude"] = g.Venue.Latitude
			fields["venue_longitude"] = g.Venue.Longitude
		}

		if g.Festival != nil {
			tags["festival_name"] = g.Festival.Name

			fields["festival_name"] = g.Festival.Name

			if g.Festival.Start != nil {
				fields["festival_start"] = g.Festival.Start.Format(store.DateFormat)
			}

			if g.Festival.End != nil {
				fields["festival_start"] = g.Festival.End.Format(store.DateFormat)
			}
		}

		t := time.Now()

		if g.Date != nil {
			t = *g.Date
		}

		pt, err := client.NewPoint("gig", tags, fields, t)

		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		bp.AddPoint(pt)

		if err := g.Command.DB.InfluxDB.Write(bp); err != nil {
			fmt.Println(err.Error())
			continue
		}

		fmt.Printf("[gig] %s at %s (for %s) on %s\n",
			b,
			g.Venue.Name,
			g.Festival.Name,
			g.Date.Format(store.DateFormat),
		)
	}
}

func (g *Gig) BulkAdd() {
	rows := getCSV(g.Command.BulkFileName)

	for i, line := range rows {
		if i == 0 {
			continue
		}

		if len(line) < 3 {
			fmt.Println("line is too short, skipping")
			continue
		}

		date := parseUserDate(line[2])

		gig := &Gig{
			Command:  g.Command,
			Bands:    []string{line[0]},
			Venue:    &store.Venue{Name: line[1]},
			Festival: &store.Festival{Name: line[3]},
			Date:     date,
		}

		gig.Add()
	}
}

func (g *Gig) Delete() {}
func (g *Gig) Edit()   {}

// Parse parses the user's gig sub-command
func (g *Gig) Parse() {
	var rawDate string

	g.Venue = &store.Venue{}
	g.Festival = &store.Festival{}

	cmd := flag.NewFlagSet("gig add", flag.ExitOnError)
	cmd.StringVar(&g.Venue.Name, "venue", "", "the name of the venue the gig was at")
	cmd.StringVar(&rawDate, "date", "", fmt.Sprintf("the date of the gig (%s)", store.HintDateFormat))
	cmd.StringVar(&g.Festival.Name, "festival", "", "the name of the festival the gig was at")
	cmd.Float64Var(&g.Rating, "rating", 0.00, "rating (0.00 to 5.00)")
	cmd.StringVar(&g.Command.BulkFileName, "csv_filename", "", "bulk CSV data")

	err := cmd.Parse(os.Args[3:])

	if err != nil {
		panic(err)
	}

	g.Bands = cmd.Args()
	g.Date = parseUserDate(rawDate)
}
