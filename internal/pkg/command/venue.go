package command

import (
	"flag"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/n7st/bandtracker/internal/pkg/store"
)

// Venue describes a gig venue, and can be created from command line arguments
// or loaded from the Bolt database.
type Venue struct {
	*Command

	Venue *store.Venue
}

// Add stores a new venue in the local Bolt database
func (v *Venue) Add() {
	venue, err := v.DB.GetVenue(v.Venue.Name)

	if err != nil {
		panic(err) // TODO
	}

	if venue == nil {
		err = v.DB.SetVenue(v.Venue)

		if err != nil {
			panic(err) // TODO
		}

		venue = v.Venue
	}

	fmt.Printf("[venue] name: %s, latitude: %.2f, longitude: %.2f\n",
		venue.Name,
		venue.Latitude,
		venue.Longitude,
	)
}

// BulkAdd creates venues in bulk from a CSV file.
// Required columns:
// - Name
// - Latitude
// - Longitude
func (v *Venue) BulkAdd() {
	rows := getCSV(v.Command.BulkFileName)

	for i, line := range rows {
		if i == 0 {
			// Skip header row
			continue
		}

		var (
			err      error
			lat, lon float64
		)

		if lat, err = strconv.ParseFloat(line[1], 64); err != nil {
			panic(err) // TODO
		}

		if lon, err = strconv.ParseFloat(line[2], 64); err != nil {
			panic(err) // TODO
		}

		venue := &Venue{
			Command: v.Command,
			Venue: &store.Venue{
				Name:      line[0],
				Latitude:  lat,
				Longitude: lon,
			},
		}

		venue.Add() // Reuse the "Add" command to insert the venue
	}
}

func (v *Venue) Delete() {}
func (v *Venue) Edit()   {}

// Parse parses the user's venue sub-command
func (v *Venue) Parse() {
	cmd := flag.NewFlagSet("venue add", flag.ExitOnError)
	v.Venue = &store.Venue{}

	cmd.StringVar(&v.Venue.Name, "name", "", "the venue's name")
	cmd.Float64Var(&v.Venue.Latitude, "latitude", 0.00, "the venue's latitude")
	cmd.Float64Var(&v.Venue.Longitude, "longitude", 0.00, "the venue's longitude")
	cmd.StringVar(&v.Command.BulkFileName, "csv_filename", "", "bulk CSV data")

	err := cmd.Parse(os.Args[3:])

	if err != nil {
		panic(err)
	}
}
