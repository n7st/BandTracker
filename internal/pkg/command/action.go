package command

import (
	"encoding/csv"
	"fmt"
	"os"
	"time"

	"gitlab.com/n7st/bandtracker/internal/pkg/store"
)

const (
	commandFestival = "festival"
	commandGig      = "gig"
	commandVenue    = "venue"

	subCommandAdd          = "add"
	subCommandAddShort     = "a"
	subCommandBulkAdd      = "bulk"
	subCommandBulkAddShort = "b"
	subCommandDelete       = "delete"
	subCommandDeleteShort  = "d"
	subCommandEdit         = "edit"
	subCommandEditShort    = "e"
)

// Action is the parent for commands
type Action interface {
	Add()
	BulkAdd()
	Delete()
	Edit()
	Parse()
}

type Command struct {
	DB *store.DB

	BulkFileName string
	InfluxDBName string
}

func GetCommand(name string, db *store.DB, influxDBName string) Action {
	c := &Command{DB: db, InfluxDBName: influxDBName}

	switch name {
	case commandGig:
		return &Gig{Command: c}
	case commandFestival:
		return &Festival{Command: c}
	case commandVenue:
		return &Venue{Command: c}
	default:
		return nil
	}
}

// runSubCommand triggers the user's sub-command
func RunSubCommand(a Action, subCommand string) {
	subCommands := map[string]func(){
		subCommandAdd:          a.Add,
		subCommandAddShort:     a.Add,
		subCommandBulkAdd:      a.BulkAdd,
		subCommandBulkAddShort: a.BulkAdd,
		subCommandDelete:       a.Delete,
		subCommandDeleteShort:  a.Delete,
		subCommandEdit:         a.Edit,
		subCommandEditShort:    a.Edit,
	}

	if c := subCommands[subCommand]; c != nil {
		c()
	} else {
		// Show help
	}
}

// parseUserDate converts a date string to a time object
func parseUserDate(userInput string) *time.Time {
	if userInput != "" {
		date, dateErr := time.Parse(store.DateFormat, userInput)

		if dateErr != nil {
			fmt.Printf("'%s' is not a valid ISO standard date (%s)\n", userInput, store.HintDateFormat)
			os.Exit(1)
		}

		return &date
	}

	return nil
}

func validateBulkAdd(name string) {
	if name == "" {
		fmt.Println("csv_filename is required")
		os.Exit(1)
	}
}

func getCSV(name string) [][]string {
	validateBulkAdd(name)

	f, err := os.Open(name)

	if err != nil {
		panic(err)
	}

	defer func() {
		err := f.Close()

		if err != nil {
			panic(err) // TODO
		}
	}()

	c, err := csv.NewReader(f).ReadAll()

	if err != nil {
		panic(err)
	}

	return c
}
