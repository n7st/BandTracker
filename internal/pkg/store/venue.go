package store

import (
	"go.etcd.io/bbolt"
)

type Venue struct {
	Latitude  float64
	Longitude float64
	Name      string
}

// setVenue stores a venue in the local Bolt database
func (db *DB) SetVenue(v *Venue) error {
	return db.Local.Update(func(tx *bbolt.Tx) error {
		venueBucket := tx.Bucket([]byte("venue"))

		if venueBucket == nil {
			return bbolt.ErrBucketNotFound
		}

		bucket, err := venueBucket.CreateBucketIfNotExists([]byte(v.Name))

		if err != nil {
			return err
		}

		err = bucket.Put([]byte("latitude"), float64ToBytes(v.Latitude))

		if err != nil {
			return err
		}

		return bucket.Put([]byte("longitude"), float64ToBytes(v.Longitude))
	})
}

// getVenue retrieves a venue by name from the local Bolt database
func (db *DB) GetVenue(name string) (*Venue, error) {
	v := &Venue{
		Name: name,
	}

	err := db.Local.View(func(tx *bbolt.Tx) error {
		venueBucket := tx.Bucket([]byte("venue"))

		if venueBucket == nil {
			return bbolt.ErrBucketNotFound
		}

		foundVenue := venueBucket.Bucket([]byte(name))

		if foundVenue == nil {
			v = nil
		} else {
			v.Latitude = bytesToFloat64(foundVenue.Get([]byte("latitude")))
			v.Longitude = bytesToFloat64(foundVenue.Get([]byte("longitude")))
		}

		return nil
	})

	return v, err
}
