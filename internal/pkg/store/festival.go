package store

import (
	"time"

	"go.etcd.io/bbolt"
)

const (
	bucketNameFestival = "festival"
	bucketKeyStart     = "start"
	bucketKeyEnd       = "end"
)

type Festival struct {
	End   *time.Time
	Name  string
	Start *time.Time
}

func (f *Festival) GetDates() (string, string) {
	var start, end string

	if f.Start != nil {
		start = f.Start.Format(DateFormat)
	}

	if f.End != nil {
		end = f.End.Format(DateFormat)
	}

	return start, end
}

func (db *DB) SetFestival(f *Festival) error {
	return db.Local.Update(func(tx *bbolt.Tx) error {
		festivalBucket := tx.Bucket([]byte(bucketNameFestival))

		if festivalBucket == nil {
			return bbolt.ErrBucketNotFound
		}

		bucket, err := festivalBucket.CreateBucketIfNotExists([]byte(f.Name))

		if err != nil {
			return err
		}

		start, end := f.GetDates()

		err = bucket.Put([]byte(bucketKeyStart), []byte(start))

		if err != nil {
			return err
		}

		return bucket.Put([]byte(bucketKeyEnd), []byte(end))
	})
}

func (db *DB) GetFestival(name string) (*Festival, error) {
	f := &Festival{}

	err := db.Local.View(func(tx *bbolt.Tx) error {
		festivalBucket := tx.Bucket([]byte(bucketNameFestival))

		if festivalBucket == nil {
			return bbolt.ErrBucketNotFound
		}

		foundFestival := festivalBucket.Bucket([]byte(name))

		if foundFestival == nil {
			f = nil
		} else {
			start, err := byteArrayToTime(foundFestival.Get([]byte(bucketKeyStart)))

			if err != nil {
				return err
			}

			f.Start = start

			end, err := byteArrayToTime(foundFestival.Get([]byte(bucketKeyEnd)))

			if err != nil {
				return err
			}

			f.End = end
		}

		return nil
	})

	return f, err
}
