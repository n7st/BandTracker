package store

import (
	"bytes"
	"encoding/binary"
	"errors"
	"math"
	"time"

	"github.com/influxdata/influxdb/client/v2"
	"go.etcd.io/bbolt"

	"gitlab.com/n7st/bandtracker/internal/pkg/util"
)

const (
	HintDateFormat = "YYYY-MM-DD"
	DateFormat     = "2006-01-02"
)

type DB struct {
	InfluxDB client.Client
	Local    *bbolt.DB
}

// NewDB sets up the local and remote database connections
func NewDB(c *util.Config) *DB {
	db := &DB{}

	db.getInflux(c)
	db.getLocal(c)

	return db
}

// getDB sets up connections to the remote and local database
func (db *DB) getLocal(c *util.Config) {
	b, err := bbolt.Open(c.DB.Bolt.FileSystemLocation, 0600, nil)

	if err != nil {
		panic(err)
	}

	db.Local = b

	err = db.initLocal()

	if err != nil {
		panic(err)
	}
}

// initLocal sets up the buckets required for the local database
func (db *DB) initLocal() error {
	if db.Local == nil {
		return errors.New("no local database")
	}

	return db.Local.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("venue"))

		if err != nil {
			return err
		}

		_, err = tx.CreateBucketIfNotExists([]byte("festival"))

		return err
	})
}

// getInflux sets up a connection to the remote InfluxDB database
func (db *DB) getInflux(c *util.Config) {
	ifc, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     c.DB.InfluxDB.Address,
		Username: c.DB.InfluxDB.Username,
		Password: c.DB.InfluxDB.Password,
	})

	if err != nil {
		panic(err)
	}

	db.InfluxDB = ifc
}

// float64ToBytes converts a float64 into a byte array
// https://stackoverflow.com/a/43693916
func float64ToBytes(f float64) []byte {
	var buf bytes.Buffer

	err := binary.Write(&buf, binary.BigEndian, f)

	if err != nil {
		panic(err)
	}

	return buf.Bytes()
}

// bytesToFloat64 converts a byte array to a float64
func bytesToFloat64(b []byte) float64 {
	bits := binary.BigEndian.Uint64(b)

	return math.Float64frombits(bits)
}

func byteArrayToTime(input []byte) (*time.Time, error) {
	str := string(input)

	if str == "" {
		return nil, nil
	}

	t, err := time.Parse(DateFormat, str)

	if err != nil {
		return nil, err
	}

	return &t, nil
}
