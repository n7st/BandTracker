package util

import (
	"fmt"
	"github.com/shibukawa/configdir"
	"gopkg.in/yaml.v2"
	"os"
)

const (
	vendorName      = "netsplit"
	applicationName = "bandtracker"
	configName      = "config.yml"
)

// Config holds the application's configuration. See the README for example
// configuration and instructions on where to put it.
type Config struct {
	DB struct {
		// Bolt holds configuration values for the local database
		Bolt struct {
			FileSystemLocation string `yaml:"location"`
		} `yaml:"bolt"`

		// InfluxDB holds configuration values for the remote InfluxDB database
		InfluxDB struct {
			Address  string `yaml:"address"`
			Database string `yaml:"database"`
			Password string `yaml:"password"`
			Username string `yaml:"username"`
		} `yaml:"influxdb"`
	} `yaml:"db"`
}

// NewConfig configures the application
func NewConfig() *Config {
	c := &Config{}

	configDirs := configdir.New(vendorName, applicationName)
	f := configDirs.QueryFolderContainsFile(configName)

	if f != nil {
		data, err := f.ReadFile(configName)

		if err != nil {
			panic(err)
		}

		err = yaml.Unmarshal(data, c)

		if err != nil {
			panic(err)
		}
	} else {
		fmt.Printf("could not find configuration in vendor directory (%s)\n", configDirs.LocalPath)
		os.Exit(1)
	}

	return c
}
