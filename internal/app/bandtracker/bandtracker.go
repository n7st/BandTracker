package bandtracker

import (
	"fmt"
	"os"

	"gitlab.com/n7st/bandtracker/internal/pkg/command"
	"gitlab.com/n7st/bandtracker/internal/pkg/store"
	"gitlab.com/n7st/bandtracker/internal/pkg/util"
)

// BandTracker holds utilities common to the entire application
type BandTracker struct {
	Config *util.Config
	DB     *store.DB
}

// NewBandTracker bootstraps the application
func NewBandTracker() *BandTracker {
	c := util.NewConfig()

	return &BandTracker{
		Config: c,
		DB:     store.NewDB(c),
	}
}

func (bt *BandTracker) Run() {
	if len(os.Args) <= 1 {
		fmt.Println("a command is required (gig, venue, festival)")
		os.Exit(1)
	}

	c := command.GetCommand(os.Args[1], bt.DB, bt.Config.DB.InfluxDB.Database)

	if c != nil {
		if len(os.Args) > 2 {
			c.Parse()
			command.RunSubCommand(c, os.Args[2])
		} else {
			fmt.Println("a subcommand is required (add, delete, edit)")
			os.Exit(1)
		}
	} else {
		// TODO: help
	}
}
